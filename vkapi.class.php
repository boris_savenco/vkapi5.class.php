<?php
/**
 * class for vk.com api v5.27+
 *
 * @link https://bitbucket.org/boris_savenco/vkapi5.class.php
 * @autor Boris Savenco
 * @version 1.1
 */
class VkApi {
  var $appId;
  var $apiSecret;
  var $curlSession;
  var $accessToken;

  /**
  * initialize
  * @param integer $app_id     id of application
  * @param string  $api_secret secret key of application 
  */
  function VkApi($app_id, $api_secret) {
    $this->appId = $app_id;
    $this->apiSecret = $api_secret;

    $urlGetAccess = 'https://oauth.vk.com/access_token?client_id=' . $this->appId . '&client_secret=' . $this->apiSecret . '&v=5.27&grant_type=client_credentials';
    $this->curlSession = curl_init();
    $tRest = $this->postRequest($urlGetAccess);

    $this->accessToken = $tRest['access_token'];
  }

  /**
   * ket result of vkapi method
   * @param  string  $method             name of method
   * @param  array   [$params=false]     parametrs of method
   * @param  boolian [$isCritical=false] if api returns error script will be die else return error array
   * @return array   array 'responce' or array 'error'
   */
  public function api($method,$params=false,$isCritical=false) {
    if (!$params) $params = array(); 
    $params['client_secret'] = $this->apiSecret;
    $params['access_token'] = $this->accessToken;
    $url = 'https://api.vk.com/method/' . $method;
    $resp = $this->postRequest($url, $params);
    if(!isset($resp['response'])){
      if ($isCritical){
        $error_info = print_r($resp['error'],true);
        die('<br><b><u>vk.api request fail with error:</u> ' . $resp['error']['error_code'] . ' ' . $resp['error']['error_msg'] . '</b>');
      } else {
		$errr = print_r($resp['error'], true);
        return $errr;
      }
    }
    return $resp['response'];
  }

  protected function postRequest($reqUrl, $data = false){
    curl_setopt($this->curlSession, CURLOPT_URL, $reqUrl);
    curl_setopt($this->curlSession, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($this->curlSession, CURLOPT_PROTOCOLS, CURLPROTO_HTTPS);
    curl_setopt($this->curlSession, CURLOPT_TIMEOUT, '3');
    if ($data){
      curl_setopt($this->curlSession, CURLOPT_POST, true);
      curl_setopt($this->curlSession, CURLOPT_POSTFIELDS, $data);
    }else{
      curl_setopt($this->curlSession, CURLOPT_POSTFIELDS, '');
    }
    $result = curl_exec($this->curlSession);
    
    return json_decode($result, true);
  }
}
