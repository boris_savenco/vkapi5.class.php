<?php
/* 
	Example
*/
require_once 'vkapi.class.php';
//'1111' is Application ID, 'abc' is Secure key. You can find them on page 'Settings'.
$vk = new VkApi(1111,'abc');
//more info: http://vk.com/dev/secure.getUserLevel
$vk_result = $vk->api('secure.getUserLevel',Array('user_ids' => '4528017,100500'));

//if api returns an array then everything is fine
if(is_array($vk_result)){ 

	foreach($vk_result as $tMember){
		echo $tMember['id'] . ' - ' . $tMember['level']; 
	}

} else { //else api returns the print_r() of error
  echo 'error: <br>' . $vk_result; 
}
