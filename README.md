> #vkapi5.class.php
> php класс для работы с серверными api социальной сети [Вконтакте](https://vk.com)

---

### Пример использования

    require_once 'vkapi.class.php'; 
    $vk = new VkApi(11111, 'secret_key');
    $arrLvls = $vk->api('secure.getUserLevel', Array('user_ids' => '4528017,100500'));

### Описание функций
   
    $vk = new VkApi(APP_ID, APP_KEY);
 

Идентификатор и Защищенный ключ приложения можно узнать на странице управления приложением, в разделе "Настройки".
    
---

    $vk->api(METHOD_NAME, [ARRAY_OF_PARAMS[, IS_CRITICAL]])
   

* METHOD_NAME - имя метода.
* ARRAY_OF_PARAMS (*не обязательный*) - Массив параметров вида ['имя_параметра' => 'значение параметра']
* IS_CRITICAL (*не обязательный*) - стоит ли обрывать работу, если vkapi вернул error. Если нет (по умолчанию) будет возвращено
* **Возвращает:** Array 'responce' в случае успеха запроса или String массива 'error' в случае неудачи.
### Пример использования

Смотрите в JavaScript библиотеке для взаимодействия со списком друзей - [vk.friends.js](https://bitbucket.org/boris_savenco/vk.friends.js)
